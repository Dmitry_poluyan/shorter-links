'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('userRegistrationCtrl',['userRegistrationService','$state','$log','growl', userRegistrationCtrl]);

    function userRegistrationCtrl (userRegistrationService, $state, $log, growl) {
        var self = this;

        this.addNewUser = function (newUser, isvalid) {
            if (isvalid) {
                userRegistrationService.postRegistrationUser(newUser).then(function(){
                    $state.go('loginUser');
                    growl.success('Registration success', {title: 'Seccess!'});
                }).catch(function(rej){
                    $log.debug('userRegistrationCtrl: postRegistrationUser ERROR', {title: 'ALERT WE GOT ERROR'});
                    $log.debug(rej);
                    if((rej.statusText === 'Unprocessable Entity')&&(rej.data.error === 'User exist')){
                        //self.errorUserExist = 'User exist';
                        growl.warning('Write valid login and password', {title: 'Not valid data!'});
                    }else{
                        growl.error('This adds a error message', {title: 'ALERT WE GOT ERROR'});
                    }
                });
            }
            else {
                growl.warning('Please fill in the input field', {title: 'Warning!'});
            }
        };
    }

})();