'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('userLoginCtrl',['userLoginService', 'sessionUserService', '$rootScope', '$state', '$log','growl', userLoginCtrl]);

    function userLoginCtrl (userLoginService, sessionUserService, $rootScope, $state, $log, growl) {
        var self = this;

        this.loginUser = function (user, isvalid) {
            if (isvalid) {

                userLoginService.checkLoginUser(user).then(function(resCheckLoginUser){
                    $rootScope.userSession = resCheckLoginUser;
                    $state.go('userPage');
                    //self.showError = false;
                    growl.success('Registration success', {title: 'Seccess!'});
                }).catch(function(rej){
                    $rootScope.userSession = null;
                    //self.showError = true;
                    $log.debug('userLoginCtrl: loginUser: checkLoginUser: ERROR rej');
                    $log.debug(rej);
                    if(rej.statusText === 'Unauthorized'){
                        //self.errorUserExist = 'Unauthorized - not valid data';
                        growl.warning('Write valid login and password', {title: 'Not valid data!'});
                    }else{
                        growl.error('This adds a error message', {title: 'ALERT WE GOT ERROR'});
                    }
                });
            }
            else {
                //self.showError = true;
                growl.warning('Please fill in the input field', {title: 'Warning!'});
            }
        };

        //self.errorUserExist = '';
        //
        //this.getError = function (error) {
        //    if (angular.isDefined(error)) {
        //        if (error.required) {
        //            return 'Field not should be empty';
        //        }
        //        if (error.minlength) {
        //            return 'Min length should be 3';
        //        }
        //        if (error.maxlength) {
        //            return 'Max length 30';
        //        }else{
        //            return self.errorUserExist;
        //        }
        //    }
        //};
    }

})();