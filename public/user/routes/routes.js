'use strict';
(function () {
    angular
        .module('shorterLinks')
            .config(['$stateProvider', configUser]);

    function configUser ($stateProvider) {

        $stateProvider
            .state('loginUser', {
                url: '/loginUser',
                templateUrl: 'view/loginUser.html',
                controller:'userLoginCtrl',
                controllerAs:'userLogin',
                needAuth: false
            })
            .state('registrationUser', {
                url: '/registrationUser',
                templateUrl: 'view/registrationUser.html',
                controller: 'userRegistrationCtrl',
                controllerAs: 'userRegistration',
                needAuth: false
            });
    }
})();