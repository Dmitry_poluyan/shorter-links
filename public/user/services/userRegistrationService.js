'use strict';
(function () {
    angular
        .module('shorterLinks')
        .service('userRegistrationService',['$http', userRegistrationService]);

    function userRegistrationService ($http) {
        this.postRegistrationUser = function (user) {
            return $http({
                method: 'POST',
                url: '/api/user/registration',
                data: user
            }).then(function(res){
                return res;
            }).catch(function(reject) {
                throw reject;
            });
        };
    }

})();