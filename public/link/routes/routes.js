'use strict';
(function () {
    angular
        .module('shorterLinks')
        .config(['$stateProvider', '$urlRouterProvider', configLink]);

    function configLink ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/mainPageLinks');

        $stateProvider
            .state('mainPageLinks', {
                url: '/mainPageLinks',
                templateUrl: 'view/mainPageLinks.html',
                controller:'mainPageLinksCtrl',
                controllerAs: 'mainPageLinks',
                needAuth: false
            })
            .state('mainPageLinksDetails', {
                url: '/mainPageLinks/LinksDetails',
                templateUrl: 'view/mainPageLinksDetails.html',
                controller:'mainPageLinksDetailsCtrl',
                controllerAs: 'mainPageLinksDetails',
                needAuth: false,
                params: {
                    'link': null
                }
            })
            .state('searchLinkForTag', {
                url: '/mainPageLinks/LinksDetails/searchLinkForTag',
                templateUrl: 'view/searchLinkForTag.html',
                controller:'searchLinkForTagCtrl',
                controllerAs: 'searchLinkForTag',
                needAuth: false,
                params: {
                    'tag': null
                }
            })
            .state('userPage', {
                url: '/userPage',
                templateUrl: 'view/userPage.html',
                controller: 'userPageCtrl',
                controllerAs: 'userPage',
                needAuth: true
            })
            .state('userPageDetailsLink', {
                url: '/userPage/DetailsLink',
                templateUrl: 'view/userPageDetailsLink.html',
                controller: 'userPageDetailsLinkCtrl',
                controllerAs: 'userPageDetailsLink',
                needAuth: true,
                params: {
                    'link': null
                }
            })
            .state('userPageEditLink', {
                url: '/userPage/EditLink',
                templateUrl: 'view/userPageEditLink.html',
                controller: 'userPageEditLinkCtrl',
                controllerAs: 'userPageEditLink',
                needAuth: true,
                params: {
                    'link': null
                }
            });
    }
})();