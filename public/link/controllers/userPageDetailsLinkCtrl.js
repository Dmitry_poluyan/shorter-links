'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('userPageDetailsLinkCtrl',['$rootScope','$log','$state','mainPageLinksService', userPageDetailsLinkCtrl]);

    function userPageDetailsLinkCtrl ($rootScope, $log, $state,mainPageLinksService){
        var self = this;
        this.hostnamePort = window.location.host;
        this.userName = $rootScope.userSession.userName;

        mainPageLinksService.linkById($state.params.link._id).then(function (link) {
            self.linkDetails = link;
        }).catch(function(rej){
            $log.debug('userPageDetailsLinkCtrl: editShortLink ERROR');
            $log.debug(rej);
        });

        this.linksForTag = function(tag){
            $state.go('searchLinkForTag',{ tag: tag });
        };

        this.backOnUserPage = function () {
            $state.go('userPage');
        };
    }

})();