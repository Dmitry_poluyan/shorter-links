'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('mainPageLinksDetailsCtrl',['$log','$state', mainPageLinksDetailsCtrl]);

    function mainPageLinksDetailsCtrl ($log, $state){
        this.linkDetails = $state.params.link;
        this.hostnamePort = window.location.host;

        this.linksForTag = function(tag){
            $state.go('searchLinkForTag',{ tag: tag });
        };

        this.backOnMainPageLinks = function () {
            $state.go('mainPageLinks');
        };
    }

})();