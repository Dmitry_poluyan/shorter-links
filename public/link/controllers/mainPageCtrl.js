'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('mainPageLinksCtrl',['mainPageLinksService','$log','$state','linkPaginationService', mainPageLinksCtrl]);

    function mainPageLinksCtrl (mainPageLinksService, $log, $state, linkPaginationService) {
        var self = this,
            options;
        this.hostnamePort = window.location.host;

        linkPaginationService.setPaginationOptions(0,0,10);
        options = linkPaginationService.getPaginationOptions();

        this.currentPages = options.currentPage;

        mainPageLinksService.getAllLinks(options).then(function(data){
            self.links = data.links;
            linkPaginationService.setCountLinks(data.count);
            self.paginationList = linkPaginationService.getPaginationList();
        }).catch(function(rej){
            $log.debug('mainPageLinksCtrl: getAllLinks ERROR');
            $log.debug(rej);
        });

        this.mainPageLinksDetails = function (link) {
            $state.go('mainPageLinksDetails',{ link: link });
        };

        this.showNumPage = function (numPage) {
            options = linkPaginationService.getPageLinks(numPage);
            self.currentPages = options.currentPage;

            mainPageLinksService.getAllLinks(options).then(function(data){
                self.links = data.links;
                linkPaginationService.setCountLinks(data.count);
                self.paginationList = linkPaginationService.getPaginationList();
            }).catch(function(rej){
                $log.debug('mainPageLinksCtrl: getAllLinks ERROR');
                $log.debug(rej);
            });
        };


    }

})();