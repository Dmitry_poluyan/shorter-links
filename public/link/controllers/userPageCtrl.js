'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('userPageCtrl',['$rootScope','$log','$state','userPageLinksService','linkPaginationService','$scope','growl', userPageCtrl]);

    function userPageCtrl ($rootScope, $log, $state, userPageLinksService, linkPaginationService, $scope, growl) {
        var self = this;
        this.userName = $rootScope.userSession.userName;
        this.hostnamePort = window.location.host;

        linkPaginationService.setPaginationOptions(0,0,10);
        var options = linkPaginationService.getPaginationOptions();
        this.currentPages = options.currentPage;

        function allUserLinks(options){
            userPageLinksService.getAllUserLinks(options).then(function(data){
                self.links = data.links;
                linkPaginationService.setCountLinks(data.count);
                self.paginationList = linkPaginationService.getPaginationList();
            }).catch(function(rej){
                $log.debug('userPageCtrl: getAllUserLinks ERROR');
                $log.debug(rej);
            });
        }

        allUserLinks(options);

        this.showNumPage = function (numPage) {
            options = linkPaginationService.getPageLinks(numPage);
            self.currentPages = options.currentPage;

            userPageLinksService.getAllUserLinks(options).then(function(data){
                self.links = data.links;
                linkPaginationService.setCountLinks(data.count);
                self.paginationList = linkPaginationService.getPaginationList();
            }).catch(function(rej){
                $log.debug('mainPageLinksCtrl: getAllLinks ERROR');
                $log.debug(rej);
            });
        };

        this.userPageDetailsLink = function (link) {
            $state.go('userPageDetailsLink',{ link: link });
        };

        this.editShortLink = function (link) {
            $state.go('userPageEditLink',{ link: link });
        };

        this.deleteLink = function (linkId) {

            userPageLinksService.deleteUserLink(linkId).then(function () {
                allUserLinks(options);
                growl.success('Short link delete', {title: 'Success!'});
            }).catch(function(rej){
                $log.debug('userPageCtrl: deleteLink ERROR');
                $log.debug(rej);
            });
        };

        this.addNewLink = function (link, isvalid) {
            if (isvalid) {
                userPageLinksService.addShortLink(link).then(function(){
                    $scope.newLink = '';
                    $scope.createShortLinkForm.$setPristine();
                    allUserLinks(options);
                    growl.success('New short link created', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('userPageCtrl: checkLoginUser ERROR rej');
                    $log.debug(rej);
                    growl.error('This adds a error message', {title: 'ALERT WE GOT ERROR'});
                });
            }
            else {
                growl.warning('Please fill in the input field', {title: 'Warning!'});
            }
        };
    }

})();