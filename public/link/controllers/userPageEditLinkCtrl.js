'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('userPageEditLinkCtrl',['$log','$state','userPageLinksService', userPageEditLinkCtrl]);

    function userPageEditLinkCtrl($log, $state, userPageLinksService){
        this.linkDetails = $state.params.link;
        this.hostnamePort = window.location.host;

        this.editShortLink = function (link) {
            var linkDetails = {description: link.description, hashtags: link.hashtags};

            userPageLinksService.editShortLink(link._id, linkDetails).then(function () {
                $state.go('userPage');
            }).catch(function(rej){
                $log.debug('userPageEditLinkCtrl: editShortLink ERROR');
                $log.debug(rej);
            });
        };

        this.backOnUserPage = function () {
            $state.go('userPage');
        };
    }

})();