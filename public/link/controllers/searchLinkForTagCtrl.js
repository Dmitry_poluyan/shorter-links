'use strict';
(function () {
    angular
        .module('shorterLinks')
        .controller('searchLinkForTagCtrl',['$log','$state','mainPageLinksService', searchLinkForTagCtrl]);

    function searchLinkForTagCtrl($log, $state, mainPageLinksService){
        var self = this;
        var tag = $state.params.tag;
        this.tag = tag;
        this.hostnamePort = window.location.host;

        mainPageLinksService.searchLinksForTag(tag).then(function (resLinks) {
            self.linksForTag = resLinks;
        }).catch(function(rej){
            $log.debug('searchLinkForTagCtrl:mainPageLinksService.searchLinksForTag: ERROR');
            $log.debug(rej);
        });

        this.mainPageLinksDetails = function (link) {
            $state.go('mainPageLinksDetails',{ link: link });
        };
    }

})();