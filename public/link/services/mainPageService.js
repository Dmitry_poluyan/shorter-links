'use strict';
(function () {
    angular
        .module('shorterLinks')
        .service('mainPageLinksService',['$http', mainPageLinksService]);

    function mainPageLinksService ($http) {
        this.getAllLinks = function(options) {
            return $http({
                method: 'GET',
                url: '/api/link/links',
                params: options
            }).then(function(res){
                return res.data;
            }).catch(function(reject) {
                throw reject;
            });
        };
        this.searchLinksForTag = function(tag) {
            return $http({
                method: 'GET',
                url: '/api/link/linkForTag/'+tag
            }).then(function(res){
                return res.data.links;
            }).catch(function(reject) {
                throw reject;
            });
        };
        this.linkById= function(linkId) {
            return $http({
                method: 'get',
                url: '/api/link/linkId/'+linkId,
            }).then(function(res){
                return res.data.link;
            }).catch(function(reject) {
                throw reject;
            });
        };
    }

})();