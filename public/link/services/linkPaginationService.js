'use strict';
(function () {
    angular
        .module('shorterLinks')
        .service('linkPaginationService',['$sce', linkPaginationService]);
    function linkPaginationService($sce){
        var options = {};
        var allCountLinks;

        return{
            setPaginationOptions: function (currentPage, skipPage, itemsOnPage) {
                options = {
                    currentPage: currentPage,
                    skipPage: skipPage,
                    itemsOnPage: itemsOnPage
                };
            },
            setCountLinks: function (countLinks) {
                allCountLinks = {
                    countLinks:countLinks
                };
            },
            getPaginationOptions: function () {
                return options;
            },
            getTotalPagesNum: function () {
                return Math.ceil(allCountLinks.countLinks/options.itemsOnPage);
            },
            getPaginationList: function () {
                var pagesNum = this.getTotalPagesNum();
                var paginationList= [];
                paginationList.push({
                    name: 'prev',
                    link:'prev'
                });
                for(var i = 0; i<pagesNum; i++){
                    var nameNum = i+1;
                    paginationList.push({
                        name: nameNum,
                        link:i
                    });
                }
                paginationList.push({
                    name: 'next',
                    link:'next'
                });
                if(pagesNum>1){
                    return paginationList;
                }else{
                    return false;
                }
            },
            getPageLinks: function (num){
                if ( num === 'prev' ) {
                    num = this.getPrevPageLinks();
                }
                if ( num === 'next' ) {
                    num = this.getNextPageLinks();
                }else{
                    num = angular.isUndefined(num)?0:num;
                }
                options = {
                    currentPage: num,
                    skipPage: options.itemsOnPage*num,
                    itemsOnPage: options.itemsOnPage
                };
                return options;
            },
            getPrevPageLinks: function() {
                var prevPageNum = options.currentPage - 1;
                if ( prevPageNum < 0 ) {
                    prevPageNum = 0;
                }
                return prevPageNum;

            },
            getNextPageLinks: function() {
                var nextPageNum = options.currentPage + 1;
                var pagesNum = this.getTotalPagesNum();
                if ( nextPageNum >= pagesNum ) {
                    nextPageNum = pagesNum - 1;
                }
                return nextPageNum;
            }
        };
    }

})();