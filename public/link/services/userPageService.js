'use strict';
(function () {
    angular
        .module('shorterLinks')
        .service('userPageLinksService',['$http', userPageLinksService]);

    function userPageLinksService ($http){
        this.getAllUserLinks = function(options) {
            return $http({
                method: 'GET',
                url: '/api/link/userLinks',
                params: options
            }).then(function(res){
                return res.data;
            }).catch(function(reject) {
                throw reject;
            });
        };
        this.deleteUserLink = function(linkId) {
            return $http({
                method: 'DELETE',
                url: '/api/link/'+linkId
            }).then(function(res){
                return res;
            }).catch(function(reject) {
                throw reject;
            });
        };
        this.addShortLink = function(link) {
            return $http({
                method: 'POST',
                url: '/api/link',
                data: link
            }).then(function(res){
                return res;
            }).catch(function(reject) {
                throw reject;
            });
        };
        this.editShortLink = function(linkId, linkDetails) {
            return $http({
                method: 'PUT',
                url: '/api/link/'+linkId,
                data: linkDetails
            }).then(function(res){
                return res;
            }).catch(function(reject) {
                throw reject;
            });
        };
    }

})();