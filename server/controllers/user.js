'use strict';
var log = require('../libs/log')(module),
    userModel = require('../models/user'),
    bcryptCreatPassword = require('../libs/bcrypt');

exports.addUser = function (req, res, next) {
    if((req.body.username)&&(req.body.password)) {
        var newUser = new userModel({
            username: req.body.username,
            password: bcryptCreatPassword.createHash(req.body.password)
        });
        userModel.findOne({username: req.body.username}).then(function (user) {
            if (user) {
                var err = new Error('User exist');
                err.status = 422;
                return next(err);
            }
            return newUser.save(function (err) {
                if (err) {
                    if (err.name === 'ValidationError') {
                        err.status = 422;
                        err.message = 'Validation error';
                        return next(err);
                    } else {
                        return next(err);
                    }
                } else {
                    log.info('New user created');
                    return res.status(201).send({status: 'OK', user: newUser});
                }
            });
        }).catch(next);
    }else{
        var err = new Error('Validation error(field empty)');
        err.status = 422;
        return next(err);
    }
};

exports.userLogout = function (req, res, next) {
    req.logout();
    log.info('Logout: status OK');
    return res.status(200).send({status: 'OK'});
};

exports.sessionUser = function(req, res, next){
    userModel.findById(req.user.userSessionId).then(function (user) {
        if(user.username){
            res.status(200).send({status: 'AUTH', userSessionId: req.user.userSessionId, userName: user.username});
        }
        else{
            var err = new Error('Not found user');
            err.status = 404;
            return next(err);
        }
    }).catch(next);
};
exports.allUsers = function (req, res, next) {
    userModel.find({})
        .then(function (users) {
            return res.send({status: 'OK', links: users});
        }).catch(next);
};