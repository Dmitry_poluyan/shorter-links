'use strict';
var log = require('../libs/log')(module),
    linkModel = require('../models/links'),
    shortId = require('shortid');

exports.addLink = function (req, res, next) {

    if (req.body.realLinks) {
        var tags = req.body.hashtags;
        if (req.body.hashtags !== undefined) {
            tags = req.body.hashtags.split(/[ ,/%#?]+/);
        }
        var link = new linkModel({
            userId: req.session.passport.user,
            realLinks: req.body.realLinks,
            shortLinks: shortId(),
            description: req.body.description,
            hashtags: tags,
            clicks: '0'
        });
        link.save(function (err) {
            if (err) {
                return next(err);
            }
            log.info('ink created');
            return res.status(201).send({status: 'OK', link: link});
        });
    } else {
        var err = new Error('Validation error');
        err.status = 422;
        return next(err);
    }
};

exports.allLinks = function (req, res, next) {
    linkModel.find({}, null, {skip: req.query.skipPage, limit: req.query.itemsOnPage})
        .then(function (links) {
            return linkModel.count({})
                .then(function (count) {
                    return res.send({status: 'OK', links: links, count: count});
                });
        }).catch(next);
};

exports.linksByIdUser = function (req, res, next) {
    linkModel.find({userId: req.session.passport.user}, null, {skip: req.query.skipPage, limit: req.query.itemsOnPage})
        .then(function (links) {
            return linkModel.count({userId: req.session.passport.user})
                .then(function (count) {
                    return res.send({status: 'OK', links: links, count: count});
                });
        }).catch(next);
};
exports.updateLink = function (req, res, next) {
    linkModel.findById(req.params._id).then(function (link) {
        if ((link === null) || (link === undefined)) {
            return res.status(404).send({error: 'Not found'});
        }
        if (link.userId === req.session.passport.user) {
            var tags = req.body.hashtags;
            if (req.body.hashtags !== undefined) {
                tags = req.body.hashtags.toString().split(/[ ,]+/);
            }
            link.description = req.body.description;
            link.hashtags = tags;

            return link.save(function (err) {
                if (err) {
                    if (err.name === 'ValidationError') {
                        err.message = 'Validation error';
                        err.status = 422;
                        return next(err);
                    } else {
                        return next(err);
                    }
                }
                log.info('Link updated');
                return res.status(200).send({status: 'OK', link: link});
            });
        } else {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
    }).catch(next);
};

exports.deleteLink = function (req, res, next) {
    linkModel.findById(req.params._id).then(function (link) {
        if ((link === null) || (link === undefined)) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        if (link.userId === req.session.passport.user) {
            return link.remove(function (err) {
                if (err) {
                    return next(err);
                }
                log.info('Link removed');
                return res.status(200).send({status: 'OK'});
            });
        }
    }).catch(next);
};
exports.shortLink = function (req, res, next) {
    var conditions = {shortLinks: req.params.page};
    var update = { $inc: { clicks: 1 }};
    linkModel.findOne(conditions).then(function (link) {
        if ((link === null) || (link === undefined)) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        linkModel.update(conditions, update).then(function (res) {
            log.info('Link clicks +1');
            log.info('redirect');
        }).catch(next);
        return res.redirect(link.realLinks);
    }).catch(next);
};

exports.linkForTag = function (req, res, next) {
    linkModel.find({hashtags: req.params.tag}).then(function (links) {
        if ((links === null) || (links === undefined)||(links.length === 0)) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        log.info('Search links for tegs: send');
        return res.status(200).send({status: 'OK', links: links});
    }).catch(next);
};
exports.linkById = function (req, res, next) {
    linkModel.findById(req.params._id).then(function (link) {
        return res.send({status: 'OK', link: link});
    }).catch(next);
};