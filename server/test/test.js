'use strict';
var expect = require('chai').expect;
var supertest = require('supertest');
var app = require('../app');
var shortId = require('shortid');
var tag = 'habrahabr.ru';
describe('links', function() {

    it('should list all links', function (done) {
        supertest(app)
            .get('/api/links')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                expect(res).have.property('status', 200);
                done();
            });
    });
    it('should links for tag', function (done) {
        supertest(app)
            .get('/api/linkForTag/'+tag)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                expect(res).have.property('status', 200);
                done();
            });
    });

});
describe('users', function() {

    it('should list all users', function (done) {
        supertest(app)
            .get('/api/users')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                expect(res).have.property('status', 200);
                done();
            });
    });
    it('should registration user', function (done) {
        supertest(app)
            .post('/api/registration')
            .send({
                username: shortId()+'test',
                password: 12345
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                expect(res).have.property('status', 201);
                done();
            });
    });
    it('should login user', function (done) {
        supertest(app)
            .post('/api/login')
            .send({
                username: 'dmitry',
                password: 'dmitry'
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                expect(res).have.property('status', 200);
                done();
            });
    });

});