'use strict';
var checkAuth = require('../libs/checkAuth'),
    linksCtrl = require('../controllers/links.js'),
    log = require('../libs/log')(module);
var express = require('express');

var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/linkForTag/:tag', linksCtrl.linkForTag);

router.get('/links', linksCtrl.allLinks);

router.get('/linkId/:_id', linksCtrl.linkById);

router.get('/userLinks', checkAuth, linksCtrl.linksByIdUser);

router.post('/', checkAuth, linksCtrl.addLink);

router.put('/:_id', checkAuth, linksCtrl.updateLink);

router.delete('/:_id', checkAuth, linksCtrl.deleteLink);

module.exports = router;