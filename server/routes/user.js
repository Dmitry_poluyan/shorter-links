'use strict';
var userCtrl = require('../controllers/user.js'),
    passport = require('../libs/auth'),
    checkAuth = require('../libs/checkAuth'),
    log = require('../libs/log')(module);
var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/users', userCtrl.allUsers);
router.get('/me', checkAuth, userCtrl.sessionUser);
router.get('/logout', checkAuth, userCtrl.userLogout);

router.post('/registration', userCtrl.addUser);
router.post('/login', passport.authenticate('local', {}), function (req, res) {
    res.status(200).send({
        status: 'AUTH', userSessionId: req.user.userSessionId, userName: req.user.userNameSession
    });
});

module.exports = router;