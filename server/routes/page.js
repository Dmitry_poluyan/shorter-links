'use strict';
var linksCtrl = require('../controllers/links.js'),
    log = require('../libs/log')(module);
var express = require('express');

var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/:page', linksCtrl.shortLink);

module.exports = router;