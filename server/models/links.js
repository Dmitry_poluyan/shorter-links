'use strict';
var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

var Schema = mongoose.Schema;

var links = new Schema({
    userId: String,
    realLinks: {
        type: String,
        required: true
    },
    shortLinks: String,
    description: String,
    hashtags: [String],
    clicks: Number,
    dateCreated: {
        type: Date,
        default: Date.now
    }
});

var linksModel = mongoose.model('Links', links);

module.exports = linksModel;